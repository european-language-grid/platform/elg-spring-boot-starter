ELG Spring Boot Starter
=======================

This is a Spring Boot starter to make it as easy as possible to create ELG-compliant tools in Java using Spring Boot.
To use this starter, create a basic Spring Boot project with the appropriate dependency:

```groovy
plugins {
  id 'org.springframework.boot' version '2.6.2'
  id 'java'
}

apply plugin: 'io.spring.dependency-management'

group = 'com.example'
version = '0.0.1-SNAPSHOT'
sourceCompatibility = '1.8'

repositories {
  mavenCentral()
  // for snapshot versions of the starter, add the following repository
  // maven { url "https://gitlab.com/api/v4/groups/european-language-grid/-/packages/maven/" }
}

dependencies {
  implementation 'eu.european-language-grid:elg-spring-boot-starter:1.0.0'
  testImplementation 'org.springframework.boot:spring-boot-starter-test'

  // and whatever other dependencies you require
}
```

Then create one (or more) Spring beans with the `@ElgHandler` qualifier (typically `@Component` classes annotated with `@ElgHandler`, but
see below for alternative options).  There are two ways to register individual handler methods within an `@ElgHandler` class, you can either:

- annotate any method with `@ElgMessageHandler`, or
- implement `ElgHandlerRegistration` to register your handlers programmatically.

## Annotated handlers

The `@ElgMessageHandler` annotation can take a value which represents the path within the web
application where this handler should be exposed - by default all handlers are mapped to the URL `/process`.  Message
handler methods must:

- be `public` instance methods (not `static`)
- have as the first (or only) parameter some subclass of `eu.elg.model.Request` for the type of requests it is able to
  handle
  - if your project includes more than one handler method then their request types must not overlap - it is permissible
    to have one handler for text requests and another for audio mapped to the same path, or two handlers for text
    requests mapped to two different paths, but not two handlers on the same path that could handle the same type.
- in the case of audio requests, take a second parameter of type `Flux<DataBuffer>` supplying the actual audio data
- optionally take an `eu.elg.handler.ProgressReporter` as a second (or third, in the audio case) parameter
- return `eu.elg.model.Response` (or a subclass of that), or a reactive streams `Publisher` which emits a single
  `Response` (typically this would be a `Mono<TextsResponse>` or similar but any reactive streams `Publisher` that
  emits a single value will work).

Any exceptions thrown by the handler method (or returned by a failing `Publisher`) will be returned to the caller as a `Failure` message - use a
`HandlerException` to take full control over the ELG status message codes returned, any other exception will be
returned with the standard code `elg.service.internalError`.

```java
@Component
@ElgHandler
public class HelloWorldHandler {

  @ElgMessageHandler
  public AnnotationsResponse process(TextRequest request) throws Exception {
    return new AnnotationsResponse().withAnnotations("Hello",
            Arrays.asList(new AnnotationObject()
                    .withOffsets(0,1)
                    .withFeatures("hello", "world")));
  }
}
```

## Programmatic handler registration

Instead of using annotated methods, an `@ElgHandler` class may implement the `ElgHandlerRegistration` interface.  This is a signal to
the starter that the class will register its own handlers explicitly, and the class will _not_ be scanned for `@ElgMessageHandler` methods.
Handler beans that implement `ElgHandlerRegistration` will have their `registerHandlers` method called at startup, and can use the
supplied "registrar" object to register handlers on any path.  The supported handler method signatures are as in the `@ElgMessageHandler`
case above, and are represented as Java "functional" interfaces - typically registration will be done by using lambdas or method references
but it is possible for classes to directly implement the relevant functional interface.

Handler methods must take the relevant `Request` object as their first parameter, a second parameter of type `Flux<DataBuffer>` in the case
of audio handlers, and an optional final `ProgressReporter` parameter.  They must return either a `Response` subclass, or a `Publisher`
that emits a single `Response` (e.g. a `Mono<AnnotationsResponse>`).  As with annotated methods, any exceptions thrown by a synchronous
handler or reported via a failing `Publisher` for an asynchronous handler will be returned to the caller as a `Failure` response.

```java
@Component
@ElgHandler
public class TranslationHandler implements ElgHandlerRegistration {

  public Mono<TextsResponse> translate(TextRequest req) {
    // ...
  }

  public void registerHandlers(ElgHandlerRegistrar registrar) {
    registrar.asyncHandler("/translate", TextRequest.class, this::translate);
  }
}
```

The programmatic registration approach is particularly useful if you want to declare several different instances of the
same handler _class_ as handlers for different paths (e.g. several translator handlers for different language pairs
that differ only in the location of their model).  The following example takes advantage of the fact that the
`@ElgHandler` qualifier annotation can be specified on `@Bean` registration _methods_ as an alternative to annotating
the class definition directly.

```java
public class TranslationHandler implements ElgHandlerRegistration {

  private String sourceLang, targetLang;

  @Autowired
  private Translator translator;

  public TranslationHandler(String sourceLang, String targetLang) {
    this.sourceLang = sourceLang;
    this.targetLang = targetLang;
  }

  public Mono<TextsResponse> translate(TextRequest req) {
    return translator.translate(req, sourceLang, targetLang);
  }

  public void registerHandlers(ElgHandlerRegistrar registrar) {
    // compute the path based on the source/target languages
    registrar.asyncHandler("/translate/" + sourceLang + "/" + targetLang,
                           TextRequest.class, this::translate);
  }
}


@Configuration
public class AppConfig {
  @ElgHandler @Bean public TranslationHandler frToEn() {
    return new TranslationHandler("fr", "en");
  }
  @ElgHandler @Bean public TranslationHandler deToEn() {
    return new TranslationHandler("de", "en");
  }
}
```

## API endpoints

The starter will automatically set up web endpoints on all the paths with registered handlers, that can accept the
appropriate types of POST data - `application/json` for text and "structured text" requests, or
`multipart/form-data` for audio (where the first part named "request" is the JSON request and the second part named
"content" is the binary audio data).

For text handlers the endpoints will also allow `text/plain`, `text/html`,
`text/xml` and `application/xml` and will synthesize a suitable `TextRequest`, and for audio handlers the endpoints
will allow just `audio/{x-}wav` and `audio/mpeg`, synthesizing a suitable `AudioRequest`.  The endpoints will support
both `application/json` (just the final response) and `text/event-stream` (a stream of progress notifications followed
by a single final response) depending on the `Accept` header sent by the caller.  If the caller can accept progress
notifications then the endpoints will continue to echo the most recent progress report on a regular basis, ensuring
the connection is kept open during a long running process.  If the handler does not use a `ProgressReporter` then
the endpoint will simply return a dummy progress message of `{"percent":0}` to maintain the heartbeat effect.


The starter will respect the following configuration options, which you may wish to provide in your `application.yml`:

- `elg.progressInterval` - the delay between repeats of the most recent progress message during long-running requests.
  Note that if the handler issues a message via its `ProgressReporter` this will be relayed to the caller as soon as
  possible, but if there has not been a further progress report after the `progressInterval` then the last report
  will be repeated.  This value is interpreted as a `Duration` - plain numbers are interpreted as milliseconds, but
  a suffix of "s" can be used to specify seconds instead.  The default value is "15s".
- `elg.cors.enable` - should CORS be supported by the endpoints?  Default is true, specify this property set to
  `false` to override.
- `elg.cors.allowCredentials` - if CORS is enabled, should the pre-flight request return
  `Access-Control-Allow-Credentials: true`?  Default is true, specify this property set to `false` to override.

By default this starter runs an _Eclipse Vert.X_ embedded web server, with cleartext HTTP/2 enabled.  You can switch to a
different server implementation in the usual way by excluding the dependency on `vertx-spring-boot-starter-http` and
including another starter to replace it, but you will then need to enable HTTP/2 in the appropriate way for that
server yourself.

## Temporary storage

The ELG platform provides a "temporary storage" service where LT services can send arbitrary data files, and receive in response a URL that the caller of the service will be able to access from outside the platform in order to download the data that was stored.  This library provides a Java interface to the temporary storage service, which is available as a bean of type `TemporaryStorage` for injection into your handler.  The `TemporaryStorage` helper method returns a reactive streams `Publisher`, there is a convenience sub-interface `ReactorTemporaryStorage` that returns the concrete Project Reactor `Mono` type instead.

```java
@Component
@ElgHandler
public class ParseController {

  private Parser parser;
  private ReactorTemporaryStorage storage;

  @Autowired
  public ParseController(ReactorTemporaryStorage storage, Parser parser) {
    this.storage = storage;
    this.parser = parser;
  }

  @ElgMessageHandler
  public Mono<AnnotationsResponse> handle(TextRequest req) {
    // assume parser.parse takes text and returns a result including some annotations
    // and a byte[] PNG image of the parse structure
    return Mono.fromSupplier(() -> parser.parse(req.getContent()))
      // it takes some time to run, so run it on a separate scheduler so as
      // not to block the event loop
      .subscribeOn(Schedulers.boundedElastic())
      .flatMap(parseResult ->
        // send the PNG to the storage service, non-blocking
        storage.store(parseResult.getPng(), MediaType.IMAGE_PNG)
                // and include the resulting URI in response
                .map(uri -> new AnnotationsResponse()
                      .withAnnotations(parseResult.toAnnotations())
                      .withFeature("treeImage", uri)));
  }
}
```
