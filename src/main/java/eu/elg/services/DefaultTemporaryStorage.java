/*
 *    Copyright 2021 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.services;

import eu.elg.handler.HandlerException;
import eu.elg.model.ResponseMessage;
import eu.elg.model.StandardMessages;
import eu.elg.model.StatusMessage;
import eu.elg.model.responses.StoredResponse;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ReactiveHttpOutputMessage;
import org.springframework.web.reactive.function.BodyInserter;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.time.Duration;
import java.util.List;
import java.util.Optional;

/**
 * Default implementation of the temporary storage helper, using the standard Spring {@link WebClient}.  By default it
 * will call the storage service at <code>http://storage.elg/store</code> but this can be overridden (for example for
 * local testing) via the configuration property <code>elg.temporary-storage.url</code>.
 */
public class DefaultTemporaryStorage implements ReactorTemporaryStorage {

  private static final Logger log = LoggerFactory.getLogger(DefaultTemporaryStorage.class);

  private final WebClient httpClient;

  private final URI endpoint;

  public DefaultTemporaryStorage(WebClient client, URI endpoint) {
    this.httpClient = client;
    this.endpoint = endpoint;
  }

  @Override
  public <T, P extends Publisher<T>> Mono<URI> store(P data, Class<T> dataType, MediaType mimeType, Duration ttl) {
    return doStore(BodyInserters.fromPublisher(data, dataType), mimeType, ttl);
  }

  @Override
  public Mono<URI> store(Object data, MediaType mimeType, Duration ttl) {
    return doStore(BodyInserters.fromValue(data), mimeType, ttl);
  }

  @Override
  public URI storeSync(Object data, MediaType mimeType, Duration ttl) throws HandlerException {
    return doStore(BodyInserters.fromValue(data), mimeType, ttl).block();
  }

  protected Mono<URI> doStore(BodyInserter<?, ReactiveHttpOutputMessage> body, MediaType mimeType, Duration ttl) {
    return httpClient.post().uri(endpoint.toString(), builder -> {
              if(ttl != null) {
                builder.queryParam("ttl", ttl.getSeconds());
              }
              return builder.build();
            }).contentType(Optional.ofNullable(mimeType).orElse(
                    MediaType.APPLICATION_OCTET_STREAM)).body(body).retrieve()
            .bodyToMono(ResponseMessage.class).flatMap(m -> {
              if(m == null) {
                log.error("No response from temporary storage service");
                return Mono.error(new HandlerException(StandardMessages.elgResponseInvalid()));
              }
              if(m.getResponse() instanceof StoredResponse) {
                return Mono.just(((StoredResponse) m.getResponse()).getUri());
              } else if(m.getFailure() != null && m.getFailure().getErrors() != null) {
                List<StatusMessage> messagesList = m.getFailure().getErrors();
                return Mono.error(new HandlerException(
                        messagesList.toArray(new StatusMessage[messagesList.size()])));
              } else {
                log.error("Unknown error from temporary storage service");
                return Mono.error(new HandlerException(StandardMessages.elgServiceInternalError("storage service error")));
              }
            });
  }
}
