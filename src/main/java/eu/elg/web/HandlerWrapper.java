/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.web;

import eu.elg.handler.Handler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;

/**
 * Class wrapping up a handler method with the logger corresponding to the declared class of the containing bean (which
 * may not be the same as the actual class owning the handler method, if there are proxies involved).
 */
public class HandlerWrapper {
  public HandlerWrapper(Handler handler, Logger logger) {
    this.handler = handler;
    this.logger = logger;
  }

  public final Handler handler;
  public final Logger logger;

}
