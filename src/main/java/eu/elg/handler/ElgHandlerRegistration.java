/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.handler;

/**
 * Interface to be implemented by {@link ElgHandler} beans that want to programmatically register their handlers rather
 * than relying on discovery via the {@link ElgMessageHandler} annotation approach.  The {@link
 * #registerHandlers(ElgHandlerRegistrar)} method will be called after the bean is initialised and should register one
 * or more handlers, typically via method references:
 *
 * <pre>
 * public class TranslationHandler implements ElgHandlerRegistration {
 *
 *   public Mono&lt;TextsResponse&gt; translate(TextRequest req) {
 *     // ...
 *   }
 *
 *   public void registerHandlers(ElgHandlerRegistrar registrar) {
 *     registrar.asyncHandler("/translate", TextRequest.class, this::translate);
 *   }
 * }
 * </pre>
 * <p>
 * Use <code>handler</code> if the handler method is synchronous and returns the <code>Response</code> directly, use
 * <code>asyncHandler</code> if the handler method returns a reactive wrapper (typically a <code>Mono</code>).  The
 * programmatic registration approach is particularly useful if you want to declare several different instances of the
 * same handler class as handlers for different paths (e.g. several translator handlers for different language pairs
 * that differ only in the location of their model).
 *
 * <pre>
 * public class TranslationHandler implements ElgHandlerRegistration {
 *
 *   private String sourceLang, targetLang;
 *
 *   @Autowired
 *   private Translator translator;
 *
 *   public TranslationHandler(String sourceLang, String targetLang) {
 *     this.sourceLang = sourceLang;
 *     this.targetLang = targetLang;
 *   }
 *
 *   public Mono&lt;TextsResponse&gt; translate(TextRequest req) {
 *     return translator.translate(req, sourceLang, targetLang);
 *   }
 *
 *   public void registerHandlers(ElgHandlerRegistrar registrar) {
 *     registrar.asyncHandler("/translate/" + sourceLang + "/" + targetLang,
 *                            TextRequest.class, this::translate);
 *   }
 * }
 *
 *
 * @Configuration
 * public class AppConfig {
 *   @ElgHandler @Bean public TranslationHandler frToEn() {
 *     return new TranslationHandler("fr", "en");
 *   }
 *   @ElgHandler @Bean public TranslationHandler deToEn() {
 *     return new TranslationHandler("de", "en");
 *   }
 * }
 * </pre>
 */
public interface ElgHandlerRegistration {

  /**
   * Called after bean initialisation to register individual request handlers.
   * @param registrar the builder that accepts calls to register handlers.
   */
  public void registerHandlers(ElgHandlerRegistrar registrar);
}
