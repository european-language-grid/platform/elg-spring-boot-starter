/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.config;

import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.annotation.BeanFactoryAnnotationUtils;
import org.springframework.boot.autoconfigure.condition.ConditionOutcome;
import org.springframework.boot.autoconfigure.condition.SpringBootCondition;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.context.annotation.ConfigurationCondition;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.util.Arrays;
import java.util.Map;

/**
 * Auto-configuration condition implementing the "at least one bean with qualifier X" test.
 */
public class OnQualifierCondition extends SpringBootCondition implements ConfigurationCondition {

  @Override
  public ConditionOutcome getMatchOutcome(ConditionContext context, AnnotatedTypeMetadata metadata) {
    Map<String, Object> annotationAttributes = metadata.getAnnotationAttributes(
            ConditionalOnQualifier.class.getName());
    String qualifier = (String) annotationAttributes.get("value");
    Class<?> beanType = (Class<?>)annotationAttributes.get("beanType");
    if(Arrays.stream(BeanFactoryUtils.beanNamesForTypeIncludingAncestors(context.getBeanFactory(), beanType))
            .anyMatch((name) -> {
              return BeanFactoryAnnotationUtils.isQualifierMatch(
                      qualifier::equals, name, context.getBeanFactory());
            })) {
      return ConditionOutcome.match("Found bean(s) with qualifier " + qualifier);
    } else {
      return ConditionOutcome.noMatch("No beans with qualifier " + qualifier);
    }
  }

  @Override
  public ConfigurationPhase getConfigurationPhase() {
    return ConfigurationPhase.REGISTER_BEAN;
  }
}
