/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.handler;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>Annotation used to mark methods of {@link ElgHandler} classes that are able to handle requests from the ELG
 * infrastructure.  Annotated methods should take a parameter whose type is one of the {@link eu.elg.model.Request}
 * subclasses, and should return either a (subclass of) {@link eu.elg.model.Response} or a {@link
 * reactor.core.publisher.Mono}&lt;{@link eu.elg.model.Response}&gt; if processing will happen
 * asynchronously.</p>
 *
 * <p>Handler methods for {@link eu.elg.model.requests.AudioRequest}</p> in particular must have a second parameter
 * of type {@link reactor.core.publisher.Flux}&lt;{@link org.springframework.core.io.buffer.DataBuffer}&gt; which
 * will provide the actual audio data.</p>
 *
 * <p>Finally, any handler method may optionally take an additional parameter of type {@link ProgressReporter}, which
 * may be used to report partial progress events during a longer-running process.</p>
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ElgMessageHandler {
  /**
   * The URL path on which this handler should be exposed - the same container can expose different services of the same
   * request type on different paths, or different request types on the same path, but it is an error if an application
   * contains more than one handler mapped to the same path for the same request subclass.
   */
  public String value() default "/process";
}
