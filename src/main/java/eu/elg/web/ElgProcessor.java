/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.web;

import eu.elg.handler.Handler;
import eu.elg.handler.HandlerException;
import eu.elg.handler.ProgressReporter;
import eu.elg.model.*;
import eu.elg.model.requests.AudioRequest;
import eu.elg.model.requests.TextRequest;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.ResolvableType;
import org.springframework.core.codec.Decoder;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.DecoderHttpMessageReader;
import org.springframework.http.codec.multipart.Part;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.reactive.result.method.annotation.RequestMappingHandlerAdapter;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.lang.reflect.InvocationTargetException;
import java.time.Duration;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

/**
 * Main class containing the WebFlux functional endpoints that implement the various types of ELG API, delegating to the
 * relevant handler methods.
 */
public class ElgProcessor {

  private static final Logger log = LoggerFactory.getLogger(ElgProcessor.class);

  private static final ParameterizedTypeReference<Request<?>> REQUEST_TYPE = new ParameterizedTypeReference<Request<?>>() {
  };

  private final Function<Throwable, Mono<? extends ServerResponse>> reportParseError = (throwable) -> {
    // JSON parse error parsing request
    log.error("Error parsing request JSON", throwable);
    return ServerResponse.badRequest().contentType(MediaType.APPLICATION_JSON)
            .bodyValue(new Failure().withErrors(StandardMessages.elgRequestInvalid()).asMessage());
  };

  @Value("${elg.progressInterval:15s}")
  private Duration progressInterval;

  private ElgHandlerMapping handlerMapping;

  private Decoder<Request<?>> requestDecoder;

  @SuppressWarnings("unchecked")
  public ElgProcessor(ElgHandlerMapping elgHandlerMapping, RequestMappingHandlerAdapter rmha) {
    this.handlerMapping = elgHandlerMapping;
    requestDecoder = ((DecoderHttpMessageReader<Request<?>>) rmha.getMessageReaders().stream()
            .filter((reader -> reader instanceof DecoderHttpMessageReader
                    && reader.canRead(ResolvableType.forType(REQUEST_TYPE), MediaType.APPLICATION_JSON)))
            .findAny().orElseThrow(() -> new RuntimeException("Could not find decoder for Request type"))).getDecoder();
  }

  /**
   * Handler for multipart/form-data audio requests, expecting a part named "request" with the JSON request and a part
   * named "content" with the audio content.
   */
  public Mono<ServerResponse> processAudioMultipart(ServerRequest serverRequest, String path) {
    return serverRequest.multipartData().flatMap((multipartData) -> {
      Part requestPart = multipartData.getFirst("request");
      if(requestPart.headers().getContentType().isCompatibleWith(MediaType.APPLICATION_JSON)) {
        Mono<? extends Request<?>> requestMono = requestDecoder.decodeToMono(requestPart.content(), ResolvableType.forType(REQUEST_TYPE), requestPart.headers().getContentType(), null);
        Flux<DataBuffer> audioContent = multipartData.getFirst("content").content();
        // check that request is actually an AudioRequest
        return requestMono.flatMap((request) -> {
          if(request instanceof AudioRequest) {
            return processRequest(serverRequest, request, path, audioContent);
          } else {
            log.warn("Multipart POST only supports audio requests, but request was " + request.type());
            return ServerResponse.badRequest().contentType(MediaType.APPLICATION_JSON)
                    .bodyValue(new Failure().withErrors(StandardMessages.elgRequestTypeUnsupported(request.type())).asMessage());
          }
        }).onErrorResume(reportParseError);
      } else {
        return ServerResponse.badRequest().contentType(MediaType.APPLICATION_JSON)
                .bodyValue(new Failure().withErrors(StandardMessages.elgRequestInvalid()).asMessage());
      }
    });
  }

  /**
   * Handler for plain audio requests of posted WAV or MP3 data.  Synthesizes an {@link AudioRequest} from the request
   * content type and puts any query string parameters into the request "params", then passes on the request in the same
   * way as if it were a multipart with an explicit AudioRequest included.
   */
  public Mono<ServerResponse> processAudioOnly(ServerRequest serverRequest, String path) {
    Map<String, Object> params = Collections.unmodifiableMap(serverRequest.queryParams().toSingleValueMap());
    AudioRequest audioRequest = new AudioRequest().withParams(params);
    Optional<MediaType> contentType = serverRequest.headers().contentType();
    if(!contentType.isPresent()) {
      // no Content-Type header - this shouldn't happen as we will only get called for the right content types
      return ServerResponse.badRequest().contentType(MediaType.APPLICATION_JSON)
              .bodyValue(new Failure().withErrors(StandardMessages.elgRequestInvalid()).asMessage());
    } else if(contentType.get().getSubtype().equals("mpeg")) {
      audioRequest.setFormat(AudioRequest.Format.MP3);
    } else if(contentType.get().getSubtype().contains("wav")) {
      audioRequest.setFormat(AudioRequest.Format.LINEAR16);
    } else {
      // invalid request - again, this shouldn't happen in practice because of the routing function
      return ServerResponse.badRequest().contentType(MediaType.APPLICATION_JSON)
              .bodyValue(new Failure().withErrors(StandardMessages.elgRequestInvalid()).asMessage());
    }

    return processRequest(serverRequest, audioRequest, path, serverRequest.exchange().getRequest().getBody());
  }

  /**
   * General handler for a posted JSON request.
   */
  public Mono<ServerResponse> processJson(ServerRequest serverRequest, String path) {
    return serverRequest.bodyToMono(REQUEST_TYPE).flatMap((request) -> {
      if(request instanceof AudioRequest) {
        // special case - audio requests require multipart or plain audio post
        log.warn("JSON post does not support audio requests - audio must use multipart or pure audio content type");
        return ServerResponse.badRequest().contentType(MediaType.APPLICATION_JSON).bodyValue(
                new Failure().withErrors(StandardMessages.elgRequestTypeUnsupported(request.type())).asMessage());
      } else {
        return processRequest(serverRequest, request, path, null);
      }
    }).onErrorResume(reportParseError);
  }

  /**
   * Handler for plain (or marked-up) posted text.  Synthesizes a TextRequest from the posted content and mime type,
   * takes the query string parameters and puts them in the "params" slot in the request, then passes it on as if it
   * were a TextRequest parsed from JSON.
   */
  public Mono<ServerResponse> processText(ServerRequest serverRequest, String path) {
    return serverRequest.bodyToMono(String.class).flatMap((content) -> {
      Map<String, Object> params = Collections.unmodifiableMap(serverRequest.queryParams().toSingleValueMap());
      return processRequest(serverRequest, new TextRequest().withContent(content)
              .withMimeType(serverRequest.headers().contentType().orElse(MediaType.TEXT_PLAIN).toString())
              .withParams(params), path, null);
    });
  }

  /**
   * Common logic for all request types.  Can respond as either text/event-stream (with progress messages) or
   * application/json (just the final response or failure).  If the caller can accept an event-stream then we return
   * one, even if the handler does not itself use a {@link ProgressReporter} - we send "0%" progress reports at regular
   * intervals to keep the connection alive.
   *
   * @param serverRequest the original request, for access to the Accept headers
   * @param request       the ELG request object
   * @param path          the path to which the handler handling this request was mapped (which may not be identical to
   *                      the request path if the mapping contained wildcards)
   * @param content       for audio requests, the content, for other requests it will be null.
   * @return the response to be returned to the caller.
   */
  @SuppressWarnings({"unchecked", "rawtypes"})
  private Mono<ServerResponse> processRequest(ServerRequest serverRequest, Request<?> request, String path, Flux<DataBuffer> content) {
    // can the caller accept event-stream progress reports?
    boolean sendingProgress = serverRequest.headers().accept().contains(MediaType.TEXT_EVENT_STREAM);

    log.trace("Processing {} request at path {}", request.type(), path);

    // find the handler
    HandlerWrapper handlerWrapper = handlerMapping.handlersForPath(path).get(request.getClass());
    if(handlerWrapper == null) {
      // no handler for this request type - give up now
      ResponseMessage responseMessage = new Failure().withErrors(StandardMessages.elgRequestTypeUnsupported(request.type())).asMessage();
      responseMessage.addWarnings(request.unknownPropertyWarnings());
      return ServerResponse.badRequest().contentType(MediaType.APPLICATION_JSON)
              .bodyValue(responseMessage);
    }

    Mono<ResponseMessage> responseMono = null;

    // prepare the method call parameters
    ProgressReporter reporter = null;
    Flux<ResponseMessage> progressFlux = null;

    if(sendingProgress) {
      // we want to send progress reports
      if(handlerWrapper.handler instanceof Handler.HasProgress) {
        // ... and the handler can generate them

        // this looks complex but what we're doing is producing a Flux of progress messages which
        // - starts with an immediate zero progress message
        // - fires an event whenever the ProgressReporter is called
        // - if no events have been published within progressInterval duration, re-emit the last message as a keepalive
        ProgressReporterImpl reporterImpl = new ProgressReporterImpl();
        reporter = reporterImpl;
        progressFlux = Flux.concat(Mono.just(new Progress().withPercent(0d).asMessage()),
                Flux.create(reporterImpl::setSink, FluxSink.OverflowStrategy.LATEST)
                        .map(Progress::asMessage))
                .switchMap((msg) -> Flux.interval(Duration.ZERO, progressInterval).map((i) -> msg));
      } else {
        // handler does not generate progress, so just do the regular 0 ticks
        final ResponseMessage progressMsg = new Progress().withPercent(0d).asMessage();
        progressFlux = Flux.interval(progressInterval).map((i) -> progressMsg);
      }
    } else {
      // client can't accept event-stream
      if(handlerWrapper.handler instanceof Handler.HasProgress) {
        // ... but handler wants to send progress - give it a no-op reporter
        reporter = ((percent, statusMessage) -> {});
      }
    }

    // call the handler
    log.debug("Calling handler {} (interfaces {})", handlerWrapper.handler, Arrays.asList(handlerWrapper.handler.getClass().getInterfaces()));
    if(handlerWrapper.handler instanceof Handler.Async.AudioWithProgress) {
      responseMono = Mono.from(((Handler.Async.AudioWithProgress) handlerWrapper.handler).handle((AudioRequest)request, content, reporter)).map(Response::asMessage);
    } else if(handlerWrapper.handler instanceof Handler.Async.Audio) {
      responseMono = Mono.from(((Handler.Async.Audio) handlerWrapper.handler).handle((AudioRequest)request, content)).map(Response::asMessage);
    } else if(handlerWrapper.handler instanceof Handler.Async.GeneralWithProgress) {
      // This appears unchecked to the compiler but we know it will be safe at runtime due to the way handlers are registered
      Mono<? extends Response<?>> responseBodyMono = Mono.from(((Handler.Async.GeneralWithProgress) handlerWrapper.handler).handle(request, reporter));
      responseMono = responseBodyMono.map(Response::asMessage);
    } else if(handlerWrapper.handler instanceof Handler.Async.General) {
      // This appears unchecked to the compiler but we know it will be safe at runtime due to the way handlers are registered
      Mono<? extends Response<?>> responseBodyMono = Mono.from(((Handler.Async.General) handlerWrapper.handler).handle(request));
      responseMono = responseBodyMono.map(Response::asMessage);
    } else {
      // shouldn't happen - our ElgHandlerRegistrar implementation converts all sync handlers into async ones
      responseMono = Mono.error(new HandlerException(StandardMessages.elgRequestTypeUnsupported(request.type())));
    }

    // build the final response mono
    if(sendingProgress) {
      // sending event-stream - so result needs to be a flux that emits the progress flux until the final
      // response mono is ready, then emits that and finishes
      return ServerResponse.ok().contentType(MediaType.TEXT_EVENT_STREAM).body(progressFlux.mergeWith(responseMono)
              .onErrorResume((ex) -> {
                log.error("Exception during processing", ex);
                return Mono.just(errorResponseFromException(ex, handlerWrapper.logger.isTraceEnabled()));
              }).handle((msg, sink) -> {
                if(msg.getFailure() != null || msg.getResponse() != null) {
                  // add any request parsing warnings to the final response
                  msg.addWarnings(request.unknownPropertyWarnings());
                  sink.next(msg);
                  // stop when we get to the "final" message (Response or Failure)
                  sink.complete();
                } else {
                  sink.next(msg);
                }
              }), ResponseMessage.class);
    } else {
      // only returning the single response message
      return responseMono
              .onErrorResume((ex) -> Mono.just(errorResponseFromException(ex, handlerWrapper.logger.isTraceEnabled())))
              .flatMap((msg) -> {
                // add any request parsing warnings to the final response
                msg.addWarnings(request.unknownPropertyWarnings());
                if(msg.getFailure() != null) {
                  return ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR).contentType(MediaType.APPLICATION_JSON).bodyValue(msg);
                } else {
                  return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON).bodyValue(msg);
                }
              });
    }
  }

  /**
   * Generate a failure message from a Java error or exception.
   *
   * @param throwable  the error or exception.  If this is a {@link HandlerException} then its status messages will be
   *                   used in the failure message.  Any other throwable will be reported as a standard "internal error"
   *                   message.
   * @param stackTrace should the exception stack trace be included in the failure message "detail" section? This
   *                   parameter is ignored if the throwable is a {@link HandlerException}, it is assumed that if the
   *                   thrower wanted to include the stack trace in one of their custom status messages then they would
   *                   have done so already.
   * @return a {@link ResponseMessage} wrapping a {@link Failure}.
   */
  private ResponseMessage errorResponseFromException(Throwable throwable, boolean stackTrace) {
    StatusMessage[] statusMessages = null;
    if(throwable instanceof InvocationTargetException) {
      // unwrap
      throwable = throwable.getCause();
    }
    if(throwable instanceof HandlerException) {
      statusMessages = ((HandlerException) throwable).getStatus();
    }

    if(statusMessages == null) {
      // either the exception isn't a HandlerException, or it is but the status is null
      StatusMessage statusMessage = StandardMessages.elgServiceInternalError(throwable.getMessage());
      if(stackTrace) {
        statusMessage.withStackTrace(throwable);
      }
      statusMessages = new StatusMessage[]{statusMessage};
    }
    Failure fail = new Failure().withErrors(statusMessages);
    return fail.asMessage();
  }


  /**
   * Progress reporter that works with a {@link FluxSink} to send progress messages to a Flux.
   */
  private static class ProgressReporterImpl implements ProgressReporter {

    private volatile FluxSink<Progress> sink;

    public void setSink(FluxSink<Progress> sink) {
      this.sink = sink;
    }

    @Override
    public void reportProgress(Double percent, StatusMessage message) {
      if(sink != null && !sink.isCancelled()) {
        sink.next(new Progress().withPercent(percent).withMessage(message));
      }
    }
  }
}
