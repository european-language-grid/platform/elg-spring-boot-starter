/*
 *    Copyright 2021 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.web.reactive.function.client.WebClientAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

import java.net.URI;

@Configuration
@AutoConfigureAfter(WebClientAutoConfiguration.class)
public class ElgStorageAutoConfiguration {

  @Bean
  @ConditionalOnBean(WebClient.Builder.class)
  @ConditionalOnProperty(name = "elg.temporary-storage.enabled", matchIfMissing = true)
  public ReactorTemporaryStorage elgTemporaryStorage(@Autowired WebClient.Builder webClientBuilder,
                                                     @Value("${elg.temporary-storage.url:http://storage.elg/store}") URI endpoint) {
    return new DefaultTemporaryStorage(webClientBuilder.build(), endpoint);
  }
}
