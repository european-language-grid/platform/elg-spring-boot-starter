/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.config;

import org.springframework.context.annotation.Conditional;

import java.lang.annotation.*;

/**
 * Conditional for auto-configuration classes that checks for the presence of at least one bean with the given type and
 * qualifier value.
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(OnQualifierCondition.class)
public @interface ConditionalOnQualifier {
  /**
   * The qualifier value to search for.
   */
  String value();

  /**
   * The type which beans must be assignable to.
   */
  Class<?> beanType() default Object.class;
}
