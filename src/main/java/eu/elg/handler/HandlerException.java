/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.handler;

import eu.elg.model.StatusMessage;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.StaticMessageSource;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Exception allowing a handler to return specific {@link StatusMessage}s rather than a generic "Internal error".
 */
public class HandlerException extends RuntimeException {

  /**
   * Called by {@link eu.elg.config.ElgAutoConfiguration} to inject the message source.
   */
  public static void setMessageSourceAccessor(MessageSourceAccessor msa) {
    HandlerException.msa = msa;
  }

  // default to a message source that just uses the status.getText() string with no i18n
  private static MessageSourceAccessor msa = new MessageSourceAccessor(new StaticMessageSource());

  private StatusMessage[] status;

  /**
   * Create an exception with a given status message (which should not be null)
   *
   * @param status status message to return.
   */
  public HandlerException(StatusMessage... status) {
    super();
    this.status = status;
  }

  /**
   * Create an exception with a given status message (which should not be null), and an underlying cause.  The cause
   * will not be forwarded to the caller but may be useful for local logging in the worker process.
   *
   * @param cause the underlying exception that caused this one.
   * @param status status messages to return.
   */
  public HandlerException(Throwable cause, StatusMessage... status) {
    super(cause);
    this.status = status;
  }

  /**
   * @return the status message that should be returned to the caller.
   */
  public StatusMessage[] getStatus() {
    return status;
  }

  /**
   * Convert and concatenate all the status messages
   * @return
   */
  public String getMessage() {
    return "[" +
            (status == null ? Stream.<StatusMessage>empty() : Stream.of(status))
                    .map((msg) ->
                            msa.getMessage(msg.getCode(),
                                    msg.getParams() == null ? null : msg.getParams().toArray(),
                                    msg.getText()))
                    .collect(Collectors.joining(", ")) +
            "]";
  }
}
