/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.handler;

import org.springframework.beans.factory.annotation.Qualifier;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation that should be used to mark beans that are able to handle ELG processing requests.  Individual methods of
 * the handler bean classes must also be annotated with {@link ElgMessageHandler}.  This annotation is a {@link
 * Qualifier} so can be placed as a class-level annotation on discoverable {@link org.springframework.stereotype.Component}
 * classes, or as a method-level annotation on a {@link org.springframework.context.annotation.Bean} method in a Spring
 * configuration class.  It can even be specified in XML configuration using <code>&lt;qualifier
 * type="ElgHandler"&gt;</code>
 */
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Qualifier("ElgHandler")
public @interface ElgHandler {
}
