/*
 *    Copyright 2021 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.services;

import org.reactivestreams.Publisher;
import org.springframework.http.MediaType;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.time.Duration;

/**
 * Interface to the ELG "temporary storage" service, which allows LT services to store arbitrary data at a temporary
 * location and receive a URL which can be returned in the LT service response for the caller to download the stored
 * data.  This is a specialization of the standard interface to return {@link Mono} instead of
 * the generic {@link org.reactivestreams.Publisher}, for the convenience of LT service implementors who themselves use
 * Reactor.
 */
public interface ReactorTemporaryStorage extends TemporaryStorage {

  /**
   * Store an item of data at a URL that the caller of your service can download.
   *
   * @param data     the data to store - this must be a reactive publisher emitting objects that the Spring WebClient is
   *                 able to serialize as the requested mime type - for textual data this is typically a String, for
   *                 binary data it could be a <code>byte[]</code> or <code>ByteBuffer</code>.
   * @param dataType the type that the data publisher emits.
   * @param mimeType MIME type under which to store the data, and with which it will be served back to the caller when
   *                 downloading.  If null then it will be stored as <code>application/octet-stream</code>.
   * @param ttl      length of time for which the resulting URL is guaranteed to remain valid.  The minimum TTL is one
   *                 second, the maximum is 24 hours and anything longer than that will be capped at 24h by the storage
   *                 service.  If null the service default is used (15 minutes).
   * @return a Mono that completes successfully with the URI of the stored data, or fails with a HandlerException if the
   * upload fails for any reason.
   */
  public <T, P extends Publisher<T>> Mono<URI> store(P data, Class<T> dataType, MediaType mimeType, Duration ttl);

  /**
   * Store an item of data at a URL that the caller of your service can download.  The URL will be valid for at least 15
   * minutes.
   *
   * @param data     the data to store - this must be a reactive publisher emitting objects that the Spring WebClient is
   *                 able to serialize as the requested mime type - for textual data this is typically a String, for
   *                 binary data it could be a <code>byte[]</code> or <code>ByteBuffer</code>.
   * @param dataType the type that the data publisher emits.
   * @param mimeType MIME type under which to store the data, and with which it will be served back to the caller when
   *                 downloading.  If null then it will be stored as <code>application/octet-stream</code>.
   * @return a Mono that completes successfully with the URI of the stored data, or fails with a HandlerException if
   * the upload fails for any reason.
   */
  default public <T, P extends Publisher<T>> Mono<URI> store(P data, Class<T> dataType, MediaType mimeType) {
    return store(data, dataType, mimeType, null);
  }

  /**
   * Store an item of data at a URL that the caller of your service can download.
   *
   * @param data     the data to store - this must be a type that the Spring WebClient is able to serialize as the
   *                 requested mime type - for textual data this is typically a String, for binary data it could be a
   *                 <code>byte[]</code> or <code>ByteBuffer</code>.  If the data comes from a reactive Publisher use
   *                 {@link #store(Publisher, Class, MediaType, Duration)} instead.
   * @param mimeType MIME type under which to store the data, and with which it will be served back to the caller when
   *                 downloading.  If null then it will be stored as <code>application/octet-stream</code>.
   * @param ttl      length of time for which the resulting URL is guaranteed to remain valid.  The minimum TTL is one
   *                 second, the maximum is 24 hours and anything longer than that will be capped at 24h by the storage
   *                 service.  If null the service default is used (15 minutes).
   * @return a Mono that completes successfully with the URI of the stored data, or fails with a HandlerException
   * if the upload fails for any reason.
   */
  public Mono<URI> store(Object data, MediaType mimeType, Duration ttl);

  /**
   * Store an item of data at a URL that the caller of your service can download.  The URL will be valid for at least 15
   * minutes.
   *
   * @param data     the data to store - this must be a type that the Spring WebClient is able to serialize as the
   *                 requested mime type - for textual data this is typically a String, for binary data it could be a
   *                 <code>byte[]</code> or <code>ByteBuffer</code>.  If the data comes from a reactive Publisher use
   *                 {@link #store(Publisher, Class, MediaType)} instead.
   * @param mimeType MIME type under which to store the data, and with which it will be served back to the caller when
   *                 downloading.  If null then it will be stored as <code>application/octet-stream</code>.
   * @return a Mono that completes successfully with the URI of the stored data, or fails with a HandlerException
   * if the upload fails for any reason.
   */
  default public Mono<URI> store(Object data, MediaType mimeType) {
    return store(data, mimeType, null);
  }
}
