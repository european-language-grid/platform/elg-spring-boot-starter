/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.handler;

import eu.elg.model.Request;
import eu.elg.model.Response;
import eu.elg.model.requests.AudioRequest;
import org.reactivestreams.Publisher;
import org.springframework.core.io.buffer.DataBuffer;
import reactor.core.publisher.Flux;

/**
 * Functional interfaces for the various types of ELG message handler methods.  These types may be implemented directly
 * but will more usually be registered using method references - see the class-level documentation for {@link
 * ElgHandlerRegistration} for full examples.
 */
public interface Handler {

  /**
   * Marker interface for handler types that accept a {@link ProgressReporter}.
   */
  public static interface HasProgress {
  }

  /**
   * Holder for synchronous handlers, which return the response directly to the calling context.
   */
  public static interface Sync extends Handler {
    /**
     * Synchronous handler for audio requests without progress reporting capability.
     */
    @FunctionalInterface
    public interface Audio extends Sync {
      /**
       * Process an audio request and return a response synchronously to the caller.
       *
       * @param request the request metadata
       * @param content the actual audio content
       * @return a suitable {@link Response}
       * @throws Exception if anything goes wrong during processing.  For full control over the generated {@link
       *                   eu.elg.model.Failure} message throw a {@link HandlerException}, any other exception will be
       *                   wrapped as a general "elg.service.internalError"
       */
      public Response<?> handle(AudioRequest request, Flux<DataBuffer> content) throws Exception;
    }

    /**
     * Synchronous handler for audio requests with progress reporting capability.
     */
    @FunctionalInterface
    public interface AudioWithProgress extends Sync, HasProgress {
      /**
       * Process an audio request and return a response synchronously to the caller.
       *
       * @param request          the request metadata
       * @param content          the actual audio content
       * @param progressReporter progress reporter which can be used to send partial progress reports
       * @return a suitable {@link Response}
       * @throws Exception if anything goes wrong during processing.  For full control over the generated {@link
       *                   eu.elg.model.Failure} message throw a {@link HandlerException}, any other exception will be
       *                   wrapped as a general "elg.service.internalError"
       */
      public Response<?> handle(AudioRequest request, Flux<DataBuffer> content, ProgressReporter progressReporter) throws Exception;
    }

    /**
     * Synchronous handler for non-audio requests without progress reporting capability.
     *
     * @param <T> the supported request type
     */
    @FunctionalInterface
    public interface General<T extends Request<T>> extends Sync {
      /**
       * Process a request and return a response synchronously to the caller.
       *
       * @param request the request metadata
       * @return a suitable {@link Response}
       * @throws Exception if anything goes wrong during processing.  For full control over the generated {@link
       *                   eu.elg.model.Failure} message throw a {@link HandlerException}, any other exception will be
       *                   wrapped as a general "elg.service.internalError"
       */
      public Response<?> handle(T request) throws Exception;
    }

    /**
     * Synchronous handler for non-audio requests with progress reporting capability.
     *
     * @param <T> the supported request type
     */
    @FunctionalInterface
    public interface GeneralWithProgress<T extends Request<T>> extends Sync, HasProgress {
      /**
       * Process a request and return a response synchronously to the caller.
       *
       * @param request          the request metadata
       * @param progressReporter progress reporter which can be used to send partial progress reports
       * @return a suitable {@link Response}
       * @throws Exception if anything goes wrong during processing.  For full control over the generated {@link
       *                   eu.elg.model.Failure} message throw a {@link HandlerException}, any other exception will be
       *                   wrapped as a general "elg.service.internalError"
       */
      public Response<?> handle(T request, ProgressReporter progressReporter) throws Exception;
    }
  }

  /**
   * Holder for asynchronous handlers, which return a reactive publisher that emits the response or fails with an
   * error.
   */
  public static interface Async extends Handler {
    /**
     * Asynchronous handler for audio requests without progress reporting capability.
     */
    @FunctionalInterface
    public interface Audio extends Async {
      /**
       * Create a single-element publisher (typically a {@link reactor.core.publisher.Mono} that processes the request
       * asynchronously on subscription and emits the successful response or error.
       *
       * @param request the request metadata
       * @param content the actual audio content, as a Flux - typically the result Mono will be a reactive pipeline
       *                derived from this.
       * @return a publisher that emits a suitable {@link Response}, or completes with an error if anything goes wrong
       * during processing.  For full control over the generated {@link eu.elg.model.Failure} message, fail with a
       * {@link HandlerException}, any other exception will be wrapped as a general "elg.service.internalError".  Note
       * that this handler method should <i>not</i> throw exceptions directly - return a failing publisher instead.
       */
      public Publisher<? extends Response<?>> handle(AudioRequest request, Flux<DataBuffer> content);
    }

    /**
     * Asynchronous handler for audio requests with progress reporting capability.
     */
    @FunctionalInterface
    public interface AudioWithProgress extends Async, HasProgress {
      /**
       * Create a single-element publisher (typically a {@link reactor.core.publisher.Mono} that processes the request
       * asynchronously on subscription and emits the successful response or error.
       *
       * @param request          the request metadata
       * @param content          the actual audio content, as a Flux - typically the result Mono will be a reactive
       *                         pipeline derived from this.
       * @param progressReporter progress reporter which can be used to send partial progress reports
       * @return a publisher that emits a suitable {@link Response}, or completes with an error if anything goes wrong
       * during processing.  For full control over the generated {@link eu.elg.model.Failure} message, fail with a
       * {@link HandlerException}, any other exception will be wrapped as a general "elg.service.internalError".  Note
       * that this handler method should <i>not</i> throw exceptions directly - return a failing publisher instead.
       */
      public Publisher<? extends Response<?>> handle(AudioRequest request, Flux<DataBuffer> content, ProgressReporter progressReporter);
    }

    /**
     * Asynchronous handler for non-audio requests without progress reporting capability.
     *
     * @param <T> the supported request type
     */
    @FunctionalInterface
    public interface General<T extends Request<T>> extends Async {
      /**
       * Create a single-element publisher (typically a {@link reactor.core.publisher.Mono} that processes the request
       * asynchronously on subscription and emits the successful response or error.
       *
       * @param request the request metadata
       * @return a publisher that emits a suitable {@link Response}, or completes with an error if anything goes wrong
       * during processing.  For full control over the generated {@link eu.elg.model.Failure} message, fail with a
       * {@link HandlerException}, any other exception will be wrapped as a general "elg.service.internalError".  Note
       * that this handler method should <i>not</i> throw exceptions directly - return a failing publisher instead.
       */
      public Publisher<? extends Response<?>> handle(T request);
    }

    /**
     * Asynchronous handler for non-audio requests with progress reporting capability.
     *
     * @param <T> the supported request type
     */
    @FunctionalInterface
    public interface GeneralWithProgress<T extends Request<T>> extends Async, HasProgress {
      /**
       * Create a single-element publisher (typically a {@link reactor.core.publisher.Mono} that processes the request
       * asynchronously on subscription and emits the successful response or error.
       *
       * @param request          the request metadata
       * @param progressReporter progress reporter which can be used to send partial progress reports
       * @return a publisher that emits a suitable {@link Response}, or completes with an error if anything goes wrong
       * during processing.  For full control over the generated {@link eu.elg.model.Failure} message, fail with a
       * {@link HandlerException}, any other exception will be wrapped as a general "elg.service.internalError".  Note
       * that this handler method should <i>not</i> throw exceptions directly - return a failing publisher instead.
       */
      public Publisher<? extends Response<?>> handle(T request, ProgressReporter progressReporter);
    }
  }

}
