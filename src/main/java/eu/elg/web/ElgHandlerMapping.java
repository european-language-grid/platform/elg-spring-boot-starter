/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.web;

import eu.elg.handler.*;
import eu.elg.model.Request;
import eu.elg.model.Response;
import eu.elg.model.requests.AudioRequest;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.ResolvableType;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.util.ClassUtils;
import org.springframework.web.method.HandlerMethod;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class responsible for gathering all {@link ElgHandler} classes and their {@link ElgMessageHandler} annotated
 * methods and grouping them by path and then by request type.  The gathering process checks that there are no clashes
 * (two different handler methods on the same path that can accept the same request type).
 */
public class ElgHandlerMapping {

  private static final Logger log = LoggerFactory.getLogger(ElgHandlerMapping.class);
  private static final ResolvableType FLUX_DATA_BUFFER_TYPE = ResolvableType.forType(new ParameterizedTypeReference<Flux<DataBuffer>>() {
  });
  private static final ResolvableType RESPONSE_TYPE = ResolvableType.forClass(Response.class);
  private static final ResolvableType PUBLISHER_RESPONSE_TYPE = ResolvableType.forType(new ParameterizedTypeReference<Publisher<? extends Response<?>>>() {
  });

  /**
   * Accessor for the complete map of handlers.
   */
  public Map<String, Map<Class<? extends Request<?>>, HandlerWrapper>> getHandlersMap() {
    return handlersMap;
  }

  /**
   * Fetches the handlers mapped to a given path.
   *
   * @param path the URL path
   * @return a map from {@link Request} sub-type to the registered {@link HandlerMethod}.
   */
  public Map<Class<? extends Request<?>>, HandlerWrapper> handlersForPath(String path) {
    return handlersMap.get(path);
  }

  private Map<String, Map<Class<? extends Request<?>>, HandlerWrapper>> handlersMap = new HashMap<>();

  /**
   * Generate the handler mapping by inspecting the {@link ElgMessageHandler} annotations on methods of the given
   * beans.
   *
   * @param handlerBeans potential handler beans, i.e. bean definitions that are annotated with {@link ElgHandler}
   */
  @SuppressWarnings({"unchecked", "rawtypes"})
  public ElgHandlerMapping(List<Object> handlerBeans) {
    log.info("Initialising ELG handler mapping - found {} potential handler beans", handlerBeans.size());
    for(Object handlerBean : handlerBeans) {
      log.debug("Processing handler bean " + handlerBean);
      Logger handlerLogger = LoggerFactory.getLogger(ClassUtils.getUserClass(handlerBean));
      Registrar registrar = new Registrar(handlerLogger);
      if(handlerBean instanceof ElgHandlerRegistration) {
        // bean handles its own registration
        ((ElgHandlerRegistration) handlerBean).registerHandlers(registrar);
      } else {
        // look for @ElgMessageHandler methods and synthesize handlers from those
        for(Method m : handlerBean.getClass().getMethods()) {
          if(AnnotatedElementUtils.hasAnnotation(m, ElgMessageHandler.class)) {
            log.debug("Found handler method {}", m);
            AnnotationAttributes handlerAnnotation = AnnotatedElementUtils.getMergedAnnotationAttributes(m, ElgMessageHandler.class);
            String path = handlerAnnotation.getString("value");
            HandlerMethod hm = new HandlerMethod(handlerBean, m);
            MethodParameter[] params = hm.getMethodParameters();
            // must take one argument that is a Request
            if(params.length < 1 || !Request.class.isAssignableFrom(params[0].getParameterType())) {
              throw new RuntimeException("ElgMessageHandler methods must have some Request type as their first parameter, " + m + " not compatible.");
            }
            Class<? extends Request<?>> requestType = (Class<? extends Request<?>>)params[0].getParameterType();
            Handler handler = null;
            boolean hasProgress = false;
            int nextParam = 1;
            // specific logic for audio requests
            if(requestType.isAssignableFrom(AudioRequest.class)) {
              // second param for audio requests must be a MultipartFile for the actual audio content
              if(params.length < 2 || !ResolvableType.forMethodParameter(params[1]).isAssignableFrom(FLUX_DATA_BUFFER_TYPE)) {
                throw new RuntimeException("AudioRequest handler methods must have a second parameter of type " +
                        "Flux<DataBuffer> to receive the audio data, " + m + " not compatible.");
              }
              if(params.length > 3) {
                throw new RuntimeException("AudioRequest Handler methods must have at most three parameters, a request, a Flux<DataBuffer> and an optional ProgressReporter");
              }
              nextParam = 2;
            } else {
              if(params.length > 2) {
                throw new RuntimeException("Non-audio Handler methods must have at most two parameters, a request and an optional ProgressReporter");
              }
            }
            if(params.length == nextParam + 1) {
              if(!params[nextParam].getParameterType().isAssignableFrom(ProgressReporter.class)) {
                throw new RuntimeException("Last parameter for handler method must be of type ProgressReporter, " + m + " not compatible.");
              }
              hasProgress = true;
            }
            // check return type
            ResolvableType returnType = ResolvableType.forMethodParameter(hm.getReturnType());
            boolean async = false;
            if(RESPONSE_TYPE.isAssignableFrom(returnType)) {
              async = false;
            } else if(PUBLISHER_RESPONSE_TYPE.isAssignableFrom(returnType)) {
              async = true;
            } else {
              throw new RuntimeException("Handler method must return either a Response or a Publisher<Response> (usually a Mono), " + m + " does not");
            }
            // we have a valid handler
            if(requestType.isAssignableFrom(AudioRequest.class)) {
              if(hasProgress) {
                if(async) {
                  registrar.asyncHandler(path, (request, content, progressReporter) -> {
                    return callAsyncMethod(hm, request, content, progressReporter);
                  });
                } else {
                  registrar.asyncHandler(path, (request, content, progressReporter) -> {
                    return callSyncMethod(hm, request, content, progressReporter);
                  });
                }
              } else {
                if(async) {
                  registrar.asyncHandler(path, (request, content) -> {
                    return callAsyncMethod(hm, request, content);
                  });
                } else {
                  registrar.asyncHandler(path, (request, content) -> {
                    return callSyncMethod(hm, request, content);
                  });
                }
              }
            } else {
              if(hasProgress) {
                if(async) {
                  // using raw types to keep compiler quiet, we know this is safe at runtime
                  registrar.asyncHandler(path, (Class)requestType, (request, progressReporter) -> {
                    return callAsyncMethod(hm, request, progressReporter);
                  });
                } else {
                  registrar.asyncHandler(path, (Class)requestType, (request, progressReporter) -> {
                    return callSyncMethod(hm, request, progressReporter);
                  });
                }
              } else {
                if(async) {
                  registrar.asyncHandler(path, (Class)requestType, (request) -> {
                    return callAsyncMethod(hm, request);
                  });
                } else {
                  registrar.asyncHandler(path, (Class)requestType, (request) -> {
                    return callSyncMethod(hm, request);
                  });
                }
              }
            }
          }
        }
      }
    }
  }

  private Publisher<? extends Response<?>> callSyncMethod(HandlerMethod hm, Object... methodParams) {
    return Mono.fromCallable(() -> (Response<?>)hm.getMethod().invoke(hm.getBean(), methodParams))
            .onErrorMap((t) -> t instanceof InvocationTargetException || t instanceof UndeclaredThrowableException,
                    (t) -> t.getCause()).subscribeOn(Schedulers.boundedElastic());
  }

  private Publisher<? extends Response<?>> callAsyncMethod(HandlerMethod hm, Object... methodParams) {
    try {
      return (Publisher<? extends Response<?>>) hm.getMethod().invoke(hm.getBean(), methodParams);
    } catch(IllegalAccessException | IllegalArgumentException e) {
      return Mono.error(e);
    } catch(InvocationTargetException | UndeclaredThrowableException e) {
      return Mono.error(e.getCause());
    }
  }

  private class Registrar implements ElgHandlerRegistrar {

    private Logger logger;

    Registrar(Logger logger) {
      this.logger = logger;
    }

    public void addHandler(String path, Handler.Async handler, Class<? extends Request<?>> requestType) {
      Map<Class<? extends Request<?>>, HandlerWrapper> thisPathHandlers = handlersMap.computeIfAbsent(path, (p) -> new HashMap<>());
      if(thisPathHandlers.keySet().stream().anyMatch((t) ->
              t.isAssignableFrom(requestType) || requestType.isAssignableFrom(t))) {
        throw new RuntimeException("Multiple handler methods found for request type " + requestType.getName() + " on path " + path);
      }
      thisPathHandlers.put(requestType, new HandlerWrapper(handler, logger));
    }

    @Override
    public void handler(String path, Handler.Sync.Audio audioHandler) {
      // convert sync handler to async
      asyncHandler(path, (request, content) ->
              Mono.fromCallable(() -> audioHandler.handle(request, content))
                      .subscribeOn(Schedulers.boundedElastic()));
    }

    @Override
    public void handler(String path, Handler.Sync.AudioWithProgress audioHandler) {
      // convert sync handler to async
      asyncHandler(path, (request, content, progressReporter) ->
              Mono.fromCallable(() -> audioHandler.handle(request, content, progressReporter))
                      .subscribeOn(Schedulers.boundedElastic()));
    }

    @Override
    public <T extends Request<T>> void handler(String path, Class<T> requestType, Handler.Sync.General<T> handler) {
      // convert sync handler to async
      asyncHandler(path, requestType, (request) ->
              Mono.fromCallable(() -> handler.handle(request))
                      .subscribeOn(Schedulers.boundedElastic()));
    }

    @Override
    public <T extends Request<T>> void handler(String path, Class<T> requestType, Handler.Sync.GeneralWithProgress<T> handler) {
      // convert sync handler to async
      asyncHandler(path, requestType, (request, progressReporter) ->
              Mono.fromCallable(() -> handler.handle(request, progressReporter))
                      .subscribeOn(Schedulers.boundedElastic()));
    }

    @Override
    public void asyncHandler(String path, Handler.Async.Audio audioHandler) {
      addHandler(path, audioHandler, AudioRequest.class);
    }

    @Override
    public void asyncHandler(String path, Handler.Async.AudioWithProgress audioHandler) {
      addHandler(path, audioHandler, AudioRequest.class);
    }

    @Override
    public <T extends Request<T>> void asyncHandler(String path, Class<T> requestType, Handler.Async.General<T> handler) {
      addHandler(path, handler, requestType);
    }

    @Override
    public <T extends Request<T>> void asyncHandler(String path, Class<T> requestType, Handler.Async.GeneralWithProgress<T> handler) {
      addHandler(path, handler, requestType);
    }
  }
}
