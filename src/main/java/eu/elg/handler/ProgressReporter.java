/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.handler;

import eu.elg.model.StatusMessage;

/**
 * Object that is able to report progress for a running ELG task.  Handler methods may take a parameter of this
 * type if they want to be able to report progress events.
 */
@FunctionalInterface
public interface ProgressReporter {

  /**
   * Send a progress report for the running task.
   *
   * @param percent percentage progress number, between 0.0 and 100.0, may be null
   * @param message status message to report, may be null
   */
  void reportProgress(Double percent, StatusMessage message);
}
