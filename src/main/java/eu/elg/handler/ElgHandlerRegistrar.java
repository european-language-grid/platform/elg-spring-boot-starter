/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.handler;

import eu.elg.model.Request;

/**
 * "Builder" type object that can be used to programmatically register handlers for specific ELG message types on
 * specific URL paths.  For usage examples see the class-level documentation for {@link ElgHandlerRegistration}.
 */
public interface ElgHandlerRegistrar {

  /**
   * Register a synchronous {@link eu.elg.model.requests.AudioRequest} handler that does not send interim progress
   * reports.
   *
   * @param path         the URL path on which to listen for requests
   * @param audioHandler the handler.
   */
  public void handler(String path, Handler.Sync.Audio audioHandler);

  /**
   * Register a synchronous {@link eu.elg.model.requests.AudioRequest} handler that is able to send interim progress
   * reports.
   *
   * @param path         the URL path on which to listen for requests
   * @param audioHandler the handler.
   */
  public void handler(String path, Handler.Sync.AudioWithProgress audioHandler);

  /**
   * Register a synchronous handler for a non-audio request type, that does not send interim progress reports.
   *
   * @param path        the URL path on which to listen for requests
   * @param requestType the type of requests that this handler can accept
   * @param handler     the handler.
   * @param <T>         the request type
   */
  public <T extends Request<T>> void handler(String path, Class<T> requestType, Handler.Sync.General<T> handler);

  /**
   * Register a synchronous handler for a non-audio request type, that is able to send interim progress reports.
   *
   * @param path        the URL path on which to listen for requests
   * @param requestType the type of requests that this handler can accept
   * @param handler     the handler.
   * @param <T>         the request type
   */
  public <T extends Request<T>> void handler(String path, Class<T> requestType, Handler.Sync.GeneralWithProgress<T> handler);

  /**
   * Register an asynchronous {@link eu.elg.model.requests.AudioRequest} handler that does not send interim progress
   * reports.
   *
   * @param path         the URL path on which to listen for requests
   * @param audioHandler the handler.
   */
  public void asyncHandler(String path, Handler.Async.Audio audioHandler);

  /**
   * Register an asynchronous {@link eu.elg.model.requests.AudioRequest} handler that is able to send interim progress
   * reports.
   *
   * @param path         the URL path on which to listen for requests
   * @param audioHandler the handler.
   */
  public void asyncHandler(String path, Handler.Async.AudioWithProgress audioHandler);

  /**
   * Register an asynchronous handler for a non-audio request type, that does not send interim progress reports.
   *
   * @param path        the URL path on which to listen for requests
   * @param requestType the type of requests that this handler can accept
   * @param handler     the handler.
   * @param <T>         the request type
   */
  public <T extends Request<T>> void asyncHandler(String path, Class<T> requestType, Handler.Async.General<T> handler);

  /**
   * Register an asynchronous handler for a non-audio request type, that is able to send interim progress reports.
   *
   * @param path        the URL path on which to listen for requests
   * @param requestType the type of requests that this handler can accept
   * @param handler     the handler.
   * @param <T>         the request type
   */
  public <T extends Request<T>> void asyncHandler(String path, Class<T> requestType, Handler.Async.GeneralWithProgress<T> handler);
}
