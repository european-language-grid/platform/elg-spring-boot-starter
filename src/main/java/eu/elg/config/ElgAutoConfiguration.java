/*
 *    Copyright 2019 The European Language Grid
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package eu.elg.config;

import eu.elg.handler.ElgHandler;
import eu.elg.handler.HandlerException;
import eu.elg.model.Failure;
import eu.elg.model.Request;
import eu.elg.model.StandardMessages;
import eu.elg.model.requests.AudioRequest;
import eu.elg.model.requests.TextRequest;
import eu.elg.web.ElgHandlerMapping;
import eu.elg.web.ElgProcessor;
import io.undertow.Undertow;
import io.undertow.UndertowOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.embedded.undertow.UndertowBuilderCustomizer;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.reactive.function.server.*;
import org.springframework.web.reactive.result.method.annotation.RequestMappingHandlerAdapter;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * Auto configuration for the ELG starter - registers the relevant WebFlux endpoints if there is at least one bean with
 * the {@link eu.elg.handler.ElgHandler} qualifier registered in the application context.
 */
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Configuration
@Import({ ElgAutoConfiguration.UndertowConfig.class })
@ConditionalOnQualifier("ElgHandler")
public class ElgAutoConfiguration {

  private static final Logger log = LoggerFactory.getLogger(ElgAutoConfiguration.class);

  @Autowired
  @ElgHandler
  private List<Object> handlerBeans;

  @Autowired
  private RequestMappingHandlerAdapter requestMappingHandlerAdapter;

  /**
   * Message source to be injected into HandlerException.
   */
  @Autowired
  private ObjectProvider<MessageSource> messageSource;

  @Value("${elg.probes.alive:/elg-alive}")
  private String livenessEndpoint;

  /**
   * Enable HTTP/2 for undertow.
   */
  @Configuration
  @ConditionalOnClass(Undertow.class)
  public static class UndertowConfig {
    @Bean
    public UndertowBuilderCustomizer elgUndertowCustomizer() {
      return (builder -> {
        builder.setServerOption(UndertowOptions.ENABLE_HTTP2, true);
      });
    }
  }

  @Bean
  public ElgHandlerMapping elgHandlerMapping() {
    return new ElgHandlerMapping(handlerBeans);
  }

  @Bean
  public ElgProcessor elgProcessor() {
    messageSource.ifAvailable((ms) -> HandlerException.setMessageSourceAccessor(new MessageSourceAccessor(ms)));
    return new ElgProcessor(elgHandlerMapping(), requestMappingHandlerAdapter);
  }

  /**
   * Enable CORS handling on ELG processing endpoints, if elg.cors.enable is <i>not</i> set to <code>false</code> in the
   * environment.
   *
   * @param allowCredentials should credentials be allowed - this is configured from <code>elg.cors.allowCredentials</code>
   *                         in the environment, and defaults to true.
   */
  @ConditionalOnProperty(name = "elg.cors.enable", matchIfMissing = true)
  @Bean
  @Order(Ordered.HIGHEST_PRECEDENCE)
  public CorsWebFilter corsWebFilter(@Value("${elg.cors.allowCredentials:true}") boolean allowCredentials) {
    log.debug("Building CORS filter - allowCredentials = {}", allowCredentials);
    CorsConfiguration corsConfig = new CorsConfiguration().applyPermitDefaultValues();
    corsConfig.setAllowedMethods(Arrays.asList(HttpMethod.POST.name(), HttpMethod.OPTIONS.name()));
    if(allowCredentials) corsConfig.setAllowCredentials(allowCredentials);
    UrlBasedCorsConfigurationSource configSource = new UrlBasedCorsConfigurationSource();
    elgHandlerMapping().getHandlersMap().forEach((path, handlers) -> {
      log.debug("Adding cors config for path {}", path);
      configSource.registerCorsConfiguration(path, corsConfig);
    });
    return new CorsWebFilter(configSource);
  }

  /**
   * Builds the ELG router based on the discovered {@link eu.elg.handler.ElgMessageHandler} methods.
   */
  @Bean
  public RouterFunction<ServerResponse> elgRouter() {
    ElgHandlerMapping handlerMapping = elgHandlerMapping();
    ElgProcessor processor = elgProcessor();
    RequestPredicate acceptJsonOrSSE = RequestPredicates.accept(MediaType.APPLICATION_JSON, MediaType.TEXT_EVENT_STREAM);
    RouterFunctions.Builder route = RouterFunctions.route();
    handlerMapping.getHandlersMap().forEach((path, handlersForPath) -> {
      Set<Class<? extends Request<?>>> handledRequestTypes = handlersForPath.keySet();
      if(handledRequestTypes.stream().anyMatch((cls) -> cls.isAssignableFrom(AudioRequest.class))) {
        // we have an audio handler on this path, so include acceptors for multipart and plain audio
        route.POST(path, RequestPredicates.contentType(MediaType.MULTIPART_FORM_DATA).and(acceptJsonOrSSE),
                (request) -> processor.processAudioMultipart(request, path));
        route.POST(path, RequestPredicates.contentType(MediaType.parseMediaType("audio/x-wav"),
                MediaType.parseMediaType("audio/wav"), MediaType.parseMediaType("audio/mpeg")).and(acceptJsonOrSSE),
                (request) -> processor.processAudioOnly(request, path));
      }

      if(handledRequestTypes.stream().anyMatch((cls) -> !cls.isAssignableFrom(AudioRequest.class))) {
        // if we have any non-audio then we need the plain JSON handler
        route.POST(path, RequestPredicates.contentType(MediaType.APPLICATION_JSON).and(acceptJsonOrSSE),
                (request) -> processor.processJson(request, path));
      }

      if(handledRequestTypes.stream().anyMatch((cls) -> cls.isAssignableFrom(TextRequest.class))) {
        // if we have text handler then add a plain text endpoint as well
        route.POST(path, RequestPredicates.contentType(MediaType.TEXT_PLAIN, MediaType.TEXT_HTML,
                MediaType.TEXT_XML, MediaType.APPLICATION_XML).and(acceptJsonOrSSE),
                (request) -> processor.processText(request, path));
      }

      // catch all handler for any other content types, to return a proper ELG error message
      route.POST(path, RequestPredicates.contentType(MediaType.ALL), (request) ->
              ServerResponse.badRequest().contentType(MediaType.APPLICATION_JSON)
                      .bodyValue(new Failure().withErrors(StandardMessages.elgRequestInvalid()).asMessage()));
    });

    // register liveness endpoint
    route.GET(livenessEndpoint, (request) -> ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
            .bodyValue("{\"alive\":true}"));

    return route.build();
  }

}
